(function iamStrict() {
    "use strict";
  
    $(document).ready(function () {
  

        var $portfolioIsotope = $('.works-wrapper').isotope({
            itemSelector: '.works-item',
            percentPosition: true,
            masonry: {
              columnWidth: '.works-item'
            }
          })
      
          // Connected items between button and isotope item
          $('.tab-menu').on('click', 'button', function () {
            var filterValue = $(this).attr('data-filter');
            $portfolioIsotope.isotope({
              filter: filterValue
            });
          });
      
          // Button active class added for isotope
          $('.tab-menu').each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'button', function () {
              $buttonGroup.find('.active_portfolio').removeClass('active_portfolio');
              $(this).addClass('active_portfolio');
            });
          });
     
  
      $('.opinion_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        infinite: true,
        asNavFor: '.opinion_nav',
        nextArrow: `<button class="slick-next slick-arrow"><svg
        class="arrow arrow-right"
        width="8"
        height="13"
        viewBox="0 0 8 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M1.49213 0.48752L0.18704 1.88987L4.47503 6.49714L0.18704 11.1056L1.49213 12.5084L7.08508 6.49714L1.49213 0.48752Z"
          fill="#BBBBBB"
        />
      </svg></button>
`,
        prevArrow: `<button class="slick-prev slick-arrow"><svg
        class="arrow arrow-left"
        width="8"
        height="13"
        viewBox="0 0 8 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M6.50764 12.5076L7.81261 11.1056L3.52463 6.49631L7.81261 1.88987L6.50764 0.486695L0.91457 6.49631L6.50764 12.5076Z"
          fill="#BBBBBB"
        />
      </svg></button>
`,
        autoplay: false,
        autoplaySpeed: 2000,
        pauseOnFocus: false,
        pauseOnHover: false,
      });
      $('.opinion_nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.opinion_slider',
        centerMode: true,
        focusOnSelect: true,
        prevArrow: `<button class="slick-prev slick-arrow"><svg
        class="arrow arrow-left"
        width="8"
        height="13"
        viewBox="0 0 8 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M6.50764 12.5076L7.81261 11.1056L3.52463 6.49631L7.81261 1.88987L6.50764 0.486695L0.91457 6.49631L6.50764 12.5076Z"
          fill="#BBBBBB"
        />
      </svg></button>
`,
        nextArrow: `<button class="slick-next slick-arrow"><svg
        class="arrow arrow-right"
        width="8"
        height="13"
        viewBox="0 0 8 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M1.49213 0.48752L0.18704 1.88987L4.47503 6.49714L0.18704 11.1056L1.49213 12.5084L7.08508 6.49714L1.49213 0.48752Z"
          fill="#BBBBBB"
        />
      </svg></button>
`,
        responsive: [{
          breakpoint: 1024,
          slidesToShow: 1,
        }]
      });
  
    });
  })(); 